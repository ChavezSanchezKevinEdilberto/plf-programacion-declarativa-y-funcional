# TEMA: "Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)"
```plantuml
@startmindmap
*[#Orange] Programación 
	*_ Se puede visualizar 
		*[#Pink] Creativa
			*[#98FB98] Ideas
			*[#98FB98] Algoritmos
			*[#98FB98] Forma de programar
			*[#98FB98] Resolución
		*[#Pink] Burocrática
			*[#98FB98] Gestión detallada de memoria
			*[#98FB98] Secuencia de órdenes
				*_ para que el ordenador 
					*[#lightblue] Ejecute las ideas
			*[#98FB98] Código máquina
		*_ Ejemplo 
			*[#Pink] Ordenadores
				*[#98FB98] Lenguaje máquina
					*_ Realizar 
						*[#lightblue] Secuencias de órdenes directas
				*[#98FB98] Creatividad 
					*[#lightblue] Nula
				*[#98FB98] Burocrática
					*[#lightblue] Gestión de memoria delicada
	*[#Pink] Programación declarativa
		*[#98FB98] Paradigma 
			*[#lightblue] Estilo de programación
			*[#lightblue] Se estudian lenguajes para cualquier tipo de aplicación

		*_ Las tareas rutinarias se dejan al 
			*[#98FB98] Compilador
				*_ Encargada de 
					*[#lightblue] Gestionar la memoria
				*_ Ventajas 
					*[#lightblue] Lenguajes declarativos
					*[#lightblue] Tiempo de modificación y verificación 
					*[#lightblue] Mejor perspectiva
					*[#lightblue] Precisión en la práctica
					*_ El estudio de programación 
						*[#lightblue] características de los lenguajes actuales
						*[#lightblue] técnicas de programación básicas
		*_ Surge como reacción a unos problemas que lleva consigo la 
			*[#98FB98] Programación clásica
				*[#lightblue] Programación imperativa
					*_ Lenguajes
						*[#FFFAFA] Fortran
							*[#3ad2ff] Compilador que traduce ordenes primitivas
						*[#FFFAFA] Modulo
						*[#FFFAFA] C
						*[#FFFAFA] Pascal
					*[#FFFAFA] Características 
						*_ El programador da 
							*[#3ad2ff] Detalles sobre los cálculos a realizar
						*[#3ad2ff] Sigue asumiendo que la máquina es tonta
						*[#3ad2ff] Cuellos de botellas
							*[#e1fdca] Tener en cuenta más detalles
						*_ Se basa en el
							*[#3ad2ff] Modelo de Von Newman
						*_ Es una 
							*[#3ad2ff] Idea abstracta
						*_ Depende del 
							*[#3ad2ff] tipo de programa a desarrollar
		*[#98FB98] Objetivos
			*[#lightblue] Liberarse de las asignaciones
			*[#lightblue] Detallar especificaciones
			*[#lightblue] Utilizar otros recursos que permitan especificar los programas a un nivel más alto
			*[#lightblue] Programas más cortos
			*[#lightblue] Fáciles de realizar
			*[#lightblue] Fáciles de depurar
		*[#98FB98] Variantes principales
			*[#lightblue] Programación funcional
				*_ Recurrir al 
					*[#FFFAFA] Lenguaje de los matemáticos
						*[#3ad2ff] Aplicación de funciones
							*[#fdcae1] Reglas de reescritura 
							*[#fdcae1] Datos de entrada y de salida
							*[#fdcae1] Reglas de simplificación
							*[#fdcae1] Aplicar funciones a datos complejos
						*[#3ad2ff] Nivel de descripción superior
				*_ Mejorar el 
					*[#FFFAFA] Razonamiento 
				*_ Es necesario 
					*[#FFFAFA] Especificar programas
				*_ Ventajas
					*[#FFFAFA] Funciones de orden superior
						*[#3ad2ff] Programas que usan otros programas 
						*[#3ad2ff] Funciones que actúan sobre funciones y no solo sobre datos primitivos
					*[#FFFAFA] Evaluación perezosa
						*[#3ad2ff] Solo se evalúan aquello que sea necesario
						*[#3ad2ff] Tipos de datos infinitos
							*_ Ejemplo
								*[#e1fdca] Serie de Fibonacci
			*[#lightblue] Programación lógica
				*[#FFFAFA] Relaciones entre objetos definidos
				*[#FFFAFA] Declarativos
				*[#FFFAFA] Modelo de demostración de la lógica
					*[#fdcae1] Lógica de predicados
					*[#fdcae1] Expresiones de programas
						*[#e1fdca] Axiomas
							*[#cccafd] Conjunto de hechos y de reglas
								*[#a88695] Reglas de inferencias
						*[#e1fdca] Compilador
							*[#cccafd] Motor de inferencia
							*[#cccafd] Razona y da respuesta a las preguntas 
					*_ Ejemplo:
						*[#fdcae1] Podemos definir una relación factorial entre dos números para determinar si un número es factorial de otro número
						*[#fdcae1] El intérprete dado el conjunto el conjunto de relaciones y predicados, intenta demostrar predicados dentro de este sistema
		*[#98FB98] Programación orientada a inteligencia artificial
			*_ Lenguaje híbrido 
				*[#lightblue] LISP
					*_ Usado en la 
						*[#FFFAFA] Programación funcional
					*_ Usado como 
						*[#FFFAFA] Modelo de programación imperativa
			*_ Lenguajes estándar 
				*[#lightblue] PROLOG 
					*_ Usado en la 
						*[#FFFAFA] Programación lógica
						*[#FFFAFA] Representación del paradigma declarativo 
				*[#lightblue] HASKELL
					*_ Usado en  
						*[#FFFAFA] Evaluación perezosa 
						*[#FFFAFA] Sistemas diferenciales
						*[#FFFAFA] Compiladores
							*[#3ad2ff] Gofer
							*[#3ad2ff] Hugs
@endmindmap
```
# TEMA: "Lenguaje de Programación Funcional (2015)"
```plantuml
@startmindmap
*[#Orange] Programación
	*[#Pink] Programación funcional
		*[#98FB98] Paradigmas de programación
			*[#lightblue] Primeros ordenadores
				*[#FFFAFA] Modelo de computación
					*[#3ad2ff] Von John Newman
						*[#e1fdca] Los programas debían almacenarse en la computadora e irse ejecutando secuencialmente
						*[#e1fdca] Modificar únicamente la instrucción de asignación
				*[#FFFAFA] Dotan de semántica los programas
			*[#lightblue] Paradigma imperativo
				*[#FFFAFA] Programación orientada a objetos
					*[#3ad2ff] Pequeños trozos de códigos
						*_ Llamados
							*[#e1fdca] Objetos
								*[#cccafd] Se componente de instrucciones
								*[#cccafd] Se ejecutan secuencialmente
			*[#lightblue] Paradigma de programación lógico
				*[#FFFAFA] Lógica simbólica
					*[#3ad2ff] Conjunto de sentencias con respecto de un programa
			*[#lightblue] Cálculo lambda
				*_ Introducido en
					*[#FFFAFA] 1930
						*_ Autores
							*[#3ad2ff] Alonzo Church
							*[#3ad2ff] Stephen Kleene
				*[#FFFAFA] Sistema formal 
					*_ Diseñado para investigar la 
						*[#3ad2ff] Definición de función
						*[#3ad2ff] Noción de aplicación de funciones 
						*[#3ad2ff] Recursión
				*[#FFFAFA] Similar al modelo de Von Newman
				*_ Surge 
					*[#FFFAFA] ALGOL
						*_ Autor 
							*[#3ad2ff] Peter J. Landin
								*[#e1fdca] 1965 
		*[#98FB98] Características
			*[#lightblue] Funciones matemáticas
				*[#FFFAFA] Consecuencias
					*[#3ad2ff] Concepto de asignación
					*[#3ad2ff] Recursión
						*_ Ejemplo
							*[#e1fdca] Factorial de un número
					*[#3ad2ff] Desaparece la definición variable
			*_ Ventajas
				*[#lightblue] Estado de computo
				*[#lightblue] Parámetros de entrada
				*[#lightblue] Devuelven el mismo valor
				*[#lightblue] Son independientes al orden del cálculo
				*[#lightblue] Transparencia referencial
		*[#98FB98] Currificación
			*_ Técnica inventada por 
				*[#lightblue] Moses Schönfinkel y Gottlob Frege
			*[#lightblue] Referencia al lógico Haskell Curry
			*[#lightblue] Aplicación parcial
				*[#FFFAFA]  Una función que recibe funciones de entradas y devuelve diferentes funciones
		*[#98FB98] Evaluación perezosa 
			*[#lightblue] Retrasa el cálculo de una expresión hasta que su valor sea necesario
			*[#lightblue] Evita repetir la evaluación en caso de ser necesaria en posteriores ocasiones
		*[#98FB98] Evaluación no estricta
			*[#lightblue] Evalúa de afuera hacia dentro
			*[#lightblue] Función que no necesita parámetros para poder desarrollarse
		*[#98FB98] Evaluación en corto circuito
			*_ Tabaja con 
				*[#lightblue] Operadores booleanos 
				*_ Algunas funciones 
					*[#FFFAFA] El segundo argumento no se ejecuta o valúa si el primer argumento de la función AND evalúa y el resultado es falso, el valor total tiene que ser falso; 
					*[#FFFAFA] El primer argumento de la función OR evalúa y el resultado es verdadero, el valor total tiene que ser verdadero
		*[#98FB98] Memoización 
			*_ Implementada en 
				*[#lightblue] 1968
					*_ por
						*[#FFFAFA] Donald Michie 
			*[#lightblue] Almacenar el valor de una expresión cuya evaluación fue realizada
		*[#98FB98] Aplicaciones
			*[#lightblue] Juegos de arcade
	*[#Pink] Programación imperativa
		*[#98FB98] Programa
			*_ Es una
				*[#lightblue] Colección de datos
				*[#lightblue] Serie de instrucciones que operan sobre dichos datos
			*[#lightblue] Se pueden modificar según el estado del cómputo
		*[#98FB98] Evaluación impaciente o estricta
			*[#lightblue] Evalúa de adentro hacia fuera
			*_ Tiempo finito para 
				*[#lightblue] Evaluar una expresión
		*[#98FB98] Lenguajes de programación
			*[#lightblue] Prototipado funcional
			*[#lightblue] Eficiencia al momento de escribir un programa
		*[#98FB98] Aplicaciones
			*[#lightblue] Resolver desde un sudoku

@endmindmap
```
## Autor: Chávez Sánchez Kevin Edilberto

[ChavezSanchezKevinEdilberto @ GitLab](https://gitlab.com/ChavezSanchezKevinEdilberto)

### Para la creación de estos Para la creación de tus mapas mentales harás uso de los formatos Markdown y PlantUML.
#### Herramientas usadas:
##### Markdown
[Markdown ](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
##### PlantUML
[Plantuml ](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
##### MindMap
[Mindmap ](https://plantuml.com/es/mindmap-diagram)